const express = require('express');
const route = express.Router();

const multer = require('../middleware/multer');
const controller = require('../controller/carsController');

const {
  userAuth,
  userLogin,
  checkRole,
  userRegister,
  serializeUser,
  deleteUser
} = require("../controller/auth");


//  ##### API CARS ##### 

// Create cars
route.post('/api/cars', userAuth, checkRole(["superadmin", "admin"]), upload.single('foto'), controller.create);

// Get All cars
route.get('/api/cars', userAuth, checkRole(["superadmin", "admin"]), controller.find);

// Get By Id cars
route.get('/api/cars/:id', userAuth, checkRole(["superadmin", "admin"]), controller.findId);

// Update cars
route.put('/api/cars/:id', userAuth, checkRole(["superadmin", "admin"]), upload.single('foto'), controller.update);

// Delete cars
route.delete('/api/cars/:id', userAuth, checkRole(["superadmin", "admin"]), controller.delete);


//  ##### API USER ##### 

// Member Registeration Route
route.post("/api/users/register-member", async (req, res) => {
  await userRegister(req.body, "member", res);
});

// Admin Registration Route
route.post("/api/users/register-admin", userAuth, checkRole(["superadmin"]), async (req, res) => {
  await userRegister(req.body, "admin", res);
});

// Super Admin Registration Route
route.post("/api/users/register-super-admin", async (req, res) => {
  await userRegister(req.body, "superadmin", res);
});

// Member Login Route
route.post("/api/users/login-member", async (req, res) => {
  await userLogin(req.body, "member", res);
});

// Admin Login Route
route.post("/api/users/login-admin", async (req, res) => {
  await userLogin(req.body, "admin", res);
});

// Super Admin Login Route
route.post("/api/users/login-super-admin", async (req, res) => {
  await userLogin(req.body, "superadmin", res);
});

// Current User
route.get("/api/users/getuser", userAuth, async (req, res) => {
  return res.json(serializeUser(req.user));
});

// Delete user 
route.delete('/api/users/:id', userAuth, checkRole(["superadmin"]), deleteUser);


module.exports = route