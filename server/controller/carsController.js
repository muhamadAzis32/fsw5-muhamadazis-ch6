var Cardb = require('../model/cars');

// create and save new 
exports.create = async (req, res) => {

    try {
        // validate request
        if (!req.body) {
            res.status(400).send({ message: "Data tidak boleh kosong!" });
            return;
        }

        // Createby


        // new data
        const {
            plate,
            manufacture,
            model,
            rentPerday,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            availableAt,
            available,
            size,
            typeDriver,
            createdBy = req.user._id,
            updatedBy = req.user._id,
        } = req.body

        const car = new Cardb({
            plate,
            manufacture,
            model,
            rentPerday,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            availableAt,
            available,
            size,
            typeDriver,
            createdBy,
            updatedBy 
        })

        // Jika upload gambar
        if (req.file) {
            const image = req.file.path;
            car.foto = image;
        }

        // Save database
        const data = await car.save()
        res.status(201).json({
            message: "successfully ",
            success: true,
            data
        });

    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while creating a create operation",
            success: false
        });
    }

}

// Gett all data
exports.find = async (req, res) => {

    try {
        const data = await Cardb.find()
        res.status(200).send({
            message: "Data ditemukan",
            success: true,
            data
        })

    } catch (err) {
        res.status(500).send({
            message: err.message || "Terjadi kesalahan saat mengambil informasi",
            success: false
        })
    }

}

// Get by id
exports.findId = async (req, res) => {

    try {
        const id = req.params.id;
        const data = await Cardb.findById(id);
        if (!data) {
            res.status(404).send({ message: "Id tidak ditemukan " + id })
        } else {
            res.status(200).send({
                message: "Id ditemukan " + id,
                success: true,
                data
            })
        }

    } catch (err) {
        res.status(500).send({
            message: "Error retrieving car with id " + id,
            success: false
        })
    }

}

// Update data
exports.update = async (req, res) => {

    try {

        if (!req.body) {
            return res
                .status(400)
                .send({ message: "Data yang akan diperbarui tidak boleh kosong" })
        }

        const id = req.params.id;
        const {
            plate,
            manufacture,
            model,
            rentPerday,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            availableAt,
            available,
            size,
            typeDriver,
            updatedBy = req.user._id,
        } = req.body

        const updates = {
            plate,
            manufacture,
            model,
            rentPerday,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            availableAt,
            available,
            size,
            typeDriver,
            updatedBy 
        };

        // Jika upload file
        if (req.file) {
            const image = req.file.path;
            updates.foto = image;
        }

        // Update database
        const data = await Cardb.findByIdAndUpdate(id, { $set: updates }, { useFindAndModify: false })
        if (!data) {
            res.status(404).send({ message: `Cannot Update car with ${id}. Maybe user not found!` })
        } else {
            res.status(200).send({
                message: `Update successfully!, UpdatedBy ${req.user._id}`,
                success: true,
            })
        }

    } catch (err) {
        res.status(500).send({
            message: "Error Update car information",
            success: false
        })
    }

}

// Delete
exports.delete = async (req, res) => {

    try {
        const id = req.params.id;
        const data = await Cardb.findByIdAndDelete(id);
        if (!data) {
            res.status(404).send({
                message: `Cannot Delete with id ${id}. Maybe id is wrong`,
                success: false
            })
        } else {
            res.send({
                message: `Car was deleted successfully!, DeletedBy ${req.user._id}`,
                success: true
            })
        }

    } catch (err) {
        res.status(500).send({
            message: "Could not delete car with id=" + id,
            success: false
        });
    }

}