const Cars = require("../model/cars");
const mongoose = require('mongoose');

// mongodb connection
mongoose.connect('mongodb://localhost:27017/dbMobil')

let cars = [
    new Cars({
        "plate": "BHD-3923",
        "manufacture": "Lincoln",
        "model": "Navigator",
        "foto": "assets/img/car08.min.jpg",
        "rentPerday": 300000,
        "capacity": 2,
        "description": "Body color sill extension. Torsion beam rear suspension",
        "availableAt": "2022-03-23T15:49:05.563Z",
        "transmission": "Automatic",
        "available": false,
        "type": "Minivan",
        "year": 2020,
        "options": "CD (Multi Disc)",
        "specs": "Body color sill extension",
        "size": "Small",
        "typeDriver": "Dengan supir"
    }),
    new Cars({
        "plate": "STL-7347",
        "manufacture": "Buick",
        "model": "LaCrosse",
        "foto": "assets/img/car01.min.jpg",
        "rentPerday": 400000,
        "capacity": 5,
        "description": "Rear reading & courtesy lamps",
        "availableAt": "2022-03-23T15:49:05.563Z",
        "transmission": "Automatic",
        "available": false,
        "type": "Extended Cab Pickup",
        "year": 2012,
        "options": "CD (Multi Disc)",
        "specs": "Rear reading & courtesy lamps",
        "size": "Small",
        "typeDriver": "Tanpa supir"
    })
];

// save database
let done = 0;
for (let i = 0; i < cars.length; i++) {
    cars[i].save(function (err, result) {
        done++;
        if (done === cars.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}