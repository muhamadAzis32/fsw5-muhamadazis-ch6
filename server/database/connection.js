const mongoose = require('mongoose');
const { success, error } = require("consola");

const connectDB = async () => {
    try {

        // mongodb connection string
        const con = await mongoose.connect(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        success({
            message: `Successfully connected with the Database \n${process.env.MONGO_URI}`,
            badge: true
        });

    } catch (err) {
        error({
            message: `Unable to connect with Database \n${err}`,
            badge: true
        });
        process.exit(1);
    }
}

module.exports = connectDB;